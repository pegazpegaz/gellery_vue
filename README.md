# image gallery

## It's a simple Vue.js gellery project based on seen idea (which has been done in jQuery)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
